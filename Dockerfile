FROM adoptopenjdk/openjdk16:alpine as builder
WORKDIR application
COPY build/libs/*.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk/openjdk16:alpine
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./

RUN adduser -D myuser
USER myuser

CMD java ${JAVA_OPTS} -Dserver.port=${PORT} -XX:+UseContainerSupport org.springframework.boot.loader.JarLauncher