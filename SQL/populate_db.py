import pandas as pandas
import psycopg2 as psycopg2

attributes = {}
attribute_values = {}
institutes = {}
high_schools = {}
students = {}
emails = {}
contacts = {}


def get_or_put(dictionary, key, calc):
    if key not in dictionary:
        dictionary[key] = calc(key)
    return dictionary.get(key)


def insert_attributes(cursor, values):
    query = """INSERT INTO attribute (title) SELECT * FROM unnest(%s) s(title) RETURNING title, id;"""
    cursor.execute(query, (values,))
    return dict(cursor.fetchall())


def insert_students(cursor, values):
    query = """
    CREATE TYPE st AS (firstname text, secondname text, patronymic text);
    INSERT INTO student (firstname, secondname, patronymic) 
    SELECT *
    FROM unnest(%s :: st[])
    RETURNING firstname, secondname, patronymic, id;
    """
    cursor.execute(query, (values,))
    res = [((a, b, c), d) for (a, b, c, d) in cursor.fetchall()]
    cursor.execute("DROP TYPE st;")
    return dict(res)


def insert_institutes(cursor, values):
    query = """INSERT INTO institute (name) 
    SELECT * FROM unnest(%s) s(name) RETURNING name, id;"""
    cursor.execute(query, (values,))
    return dict(cursor.fetchall())


def insert_schools(cursor, values):
    schools = {}
    for (inst, sc) in values:
        inst_id = institutes[inst]
        cursor.execute(
            f"""INSERT INTO high_school(institute_id, name) VALUES ({inst_id}, %s) RETURNING id;""",
            (sc,)
        )
        schools[sc] = cursor.fetchone()[0]
    return schools


def insert_attribute_values(cursor, values, attribute_name):
    attribute_id = attributes[attribute_name]
    query = f"""INSERT INTO attribute_value(attribute_id, value) SELECT {attribute_id}, unnest(%s) RETURNING value ,id;"""
    cursor.execute(
        query, (values,)
    )
    res = cursor.fetchall()
    return dict(res)


def insert_contacts(cursor, values):
    contact_type = "EMAIL"
    query = f"""INSERT INTO contact(type, address) SELECT '{contact_type}', unnest(%s) RETURNING address, id;"""
    cursor.execute(
        query, (values,)
    )
    res = cursor.fetchall()
    return dict(res)


def insert_st_attribute(cursor, student, attribute, value):
    attribute_id = attribute_values[attribute][str(value)]
    student_id = students[student]
    cursor.execute(
        "INSERT INTO student_attribute_value(attribute_value_id, student_id) VALUES (%s, %s)",
        (attribute_id, student_id)
    )


def insert_st_contact(cursor, student, contact):
    contact_id = contacts[contact]
    student_id = students[student]
    cursor.execute(
        "INSERT INTO student_contact(student_id, contact_id) VALUES (%s, %s)", (student_id, contact_id)
    )


def insert_st_school(cursor, student, school):
    student_id = students[student]
    school_id = high_schools[school]
    cursor.execute(
        "INSERT INTO student_high_school(student_id, high_school_id) VALUES (%s, %s)", (student_id, school_id)
    )


def insert_link_tables(cursor, pddf):
    for index, row in pddf.iterrows():
        student = tuple(row[student_cols].tolist())
        for col in attribute_cols:
            insert_st_attribute(cursor, student, col, row[col])
        insert_st_school(cursor, student, row[school_col])
        insert_st_contact(cursor, student, row[email_col])
        print(f"student {str(index)} {student} done")


if __name__ == "__main__":
    school_col = "Школа"
    institutes_cols = ["Институт", "Школа"]
    email_col = "Корпоративный e-mail"
    student_cols = ["Фамилия", "Имя", "Отчество"]

    df = pandas.read_excel("история_данных_о_студентах_03_10_2021_18_15_55.xlsx", header=[0])
    df = df[pandas.notnull(df[email_col])]
    df = df.where(df.notnull(), None)
    df[institutes_cols] = df["Подразделение"].str.split(" / ", expand=True)
    df = df.drop("Подразделение", axis=1)
    cols = list(df)
    attribute_cols = list(set(cols) - set(student_cols + [email_col] + institutes_cols))

    heroku_url = "ENTER URL"
    conn = psycopg2.connect(
        heroku_url,
        sslmode='require'
    )
    cur = conn.cursor()

    attributes = insert_attributes(cur, attribute_cols)
    for col in attribute_cols:
        attribute_values[col] = insert_attribute_values(cur, df[col].unique().tolist(), col)
    print("attributes done")
    institutes = insert_institutes(cur, df[institutes_cols[0]].unique().tolist())
    df["tmp_col"] = list(zip(df[institutes_cols[0]], df[institutes_cols[1]]))
    high_schools = insert_schools(cur, df["tmp_col"].unique().tolist())
    print("schools and institutes done")
    students = insert_students(cur, list(df[student_cols].itertuples(index=False, name=None)))
    print("students done")
    contacts = insert_contacts(cur, list(df[email_col].values.tolist()))
    print("contacts done")
    insert_link_tables(cur, df)
    print("link tables done")
    cur.close()
    conn.commit()
