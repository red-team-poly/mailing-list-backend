create extension if not exists citext;

create table if not exists student
(
    id         serial not null,
    firstname  text   not null,
    secondname text   not null,
    patronymic text default null,
    constraint student_pkey
        primary key (id)
);

create table if not exists user_entity
(
    id         serial  not null,
    firstname  text    not null,
    secondname text    not null,
    patronymic text default null,
    permission boolean not null,
    constraint user_pkey
        primary key (id)
);

DO
$$
    BEGIN
        CREATE TYPE user_role AS ENUM ('USER', 'ADMIN');
    EXCEPTION
        WHEN duplicate_object THEN null;
    END
$$;

create table if not exists credentials
(
    user_id   integer   not null,
    email     text      not null unique,
    password  text      not null,
    user_role user_role not null default 'USER',
    constraint credentials_user_entity_user_id_fkey
        foreign key (user_id) references user_entity
            on delete cascade
);

create table if not exists tag
(
    id            serial  not null,
    title         text    not null,
    is_public     boolean default false,
    creator_id    integer not null,
    creation_date date    default CURRENT_DATE,
    constraint tag_pkey
        primary key (id),
    constraint tag_creator_id_fkey
        foreign key (creator_id) references user_entity
            on delete cascade
);

create table if not exists student_tag
(
    student_id integer not null,
    tag_id     integer not null,
    constraint student_tag_student_id_fkey
        foreign key (student_id) references student
            on delete cascade,
    constraint student_tag_tag_id_fkey
        foreign key (tag_id) references tag
            on delete cascade
);

create table if not exists institute
(
    id   serial not null,
    name text   not null,
    constraint institute_pkey
        primary key (id)
);

create table if not exists high_school
(
    id           serial  not null,
    institute_id integer not null,
    name         text    not null,
    constraint high_school_pkey
        primary key (id),
    constraint high_school_institute_id_fkey
        foreign key (institute_id) references institute
            on delete cascade
);

create table if not exists student_high_school
(
    high_school_id integer not null,
    student_id     integer not null,
    constraint student_high_school_high_school_id_fkey
        foreign key (high_school_id) references high_school
            on delete cascade,
    constraint student_high_school_student_id_fkey
        foreign key (student_id) references student
            on delete cascade
);

create table if not exists user_high_school
(
    high_school_id integer not null,
    user_id        integer not null,
    constraint student_high_school_high_school_id_fkey
        foreign key (high_school_id) references high_school
            on delete cascade,
    constraint user_high_school_user_id_fkey
        foreign key (user_id) references user_entity
            on delete cascade
);

DO
$$
    BEGIN
        CREATE TYPE contact_type AS ENUM ('TG', 'EMAIL');
    EXCEPTION
        WHEN duplicate_object THEN null;
    END
$$;

create table if not exists contact
(
    id      serial       not null,
    address citext       not null,
    type    contact_type not null,
    constraint contact_pkey
        primary key (id)
);

create table if not exists student_contact
(
    contact_id integer not null,
    student_id integer not null,
    constraint student_contact_contact_id_fkey
        foreign key (contact_id) references contact
            on delete cascade,
    constraint student_contact_student_id_fkey
        foreign key (student_id) references student
            on delete cascade
);

create table if not exists user_contact
(
    contact_id integer not null,
    user_id    integer not null,
    constraint student_contact_contact_id_fkey
        foreign key (contact_id) references contact
            on delete cascade,
    constraint user_contact_user_id_fkey
        foreign key (user_id) references user_entity
            on delete cascade
);

create table if not exists attribute
(
    id    serial not null,
    title text   not null,
    constraint attribute_pkey
        primary key (id),
    constraint attribute_title_key
        unique (title)
);

create table if not exists attribute_value
(
    id           serial  not null,
    attribute_id integer not null,
    value        text    not null,
    constraint attribute_value_pkey
        primary key (id),
    constraint attribute_value_value_key
        unique (value),
    constraint attribute_value_attribute_id_fkey
        foreign key (attribute_id) references attribute
            on delete cascade
);

create table if not exists student_attribute_value
(
    attribute_value_id integer not null,
    student_id         integer not null,
    constraint student_attribute_value_attribute_value_id_fkey
        foreign key (attribute_value_id) references attribute_value
            on delete cascade,
    constraint student_attribute_value_student_id_fkey
        foreign key (student_id) references student
            on delete cascade
);

create table if not exists filter
(
    id                serial  not null,
    title             text    not null unique,
    is_public         boolean not null default false,
    creator_id        integer not null,
    expression        text    not null,
    creation_date     date             default CURRENT_DATE,
    human_readable_id text  not null unique,
    constraint filter_pkey
        primary key (id),
    constraint filter_creator_id_fkey
        foreign key (creator_id) references user_entity
);

create table if not exists tag_filter
(
    filter_id integer not null,
    tag_id    integer not null,
    constraint tag_filter_tag_id_fkey
        foreign key (tag_id) references tag
            on delete cascade,
    constraint tag_filter_filter_id_fkey
        foreign key (filter_id) references filter
);
