package com.readteam.mailinglistbackend.security;

import com.readteam.mailinglistbackend.model.Register;
import com.readteam.mailinglistbackend.repositories.AuthRepository;
import com.readteam.mailinglistbackend.repositories.ContactRepository;
import com.readteam.mailinglistbackend.repositories.SchoolRepository;
import com.readteam.mailinglistbackend.repositories.UserRepository;
import com.readteam.mailinglistbackend.repositories.models.*;
import org.springframework.data.util.Pair;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class CredentialsService implements ReactiveUserDetailsService, ReactiveUserDetailsPasswordService {

    private final AuthRepository authRepository;
    private final ContactRepository contactRepository;
    private final UserRepository userRepository;
    private final SchoolRepository schoolRepository;
    private final PasswordEncoder encoder;

    public CredentialsService(AuthRepository authRepository, ContactRepository contactRepository, UserRepository userRepository, SchoolRepository schoolRepository, PasswordEncoder encoder) {
        this.authRepository = authRepository;
        this.contactRepository = contactRepository;
        this.userRepository = userRepository;
        this.schoolRepository = schoolRepository;
        this.encoder = encoder;
    }


    public Mono<CredentialsWithSchools> findById(Long id) {
        return authRepository
                .getUserById(id)
                .flatMap(c -> schoolRepository
                        .getHighSchoolsByUser(c.id())
                        .collectList()
                        .map(s -> Pair.of(c, s))
                )
                .map(p -> new CredentialsWithSchools(p.getFirst(), p.getSecond()));
    }

    public Mono<Credentials> findByEmail(String email) {
        return authRepository.getUserByEmail(email);
    }

    @Transactional
    public Mono<Credentials> createUser(Register register) {
        return userRepository.save(
                        new UserEntity(null,
                                register.firstname(),
                                register.secondname(),
                                register.patronymic(),
                                false
                        )
                )
                .flatMap(u -> authRepository.insertCredentials(
                                u.id(),
                                register.email(),
                                encoder.encode(register.password())
                        )
                )
                .flatMap(c -> authRepository.getUserById(c.id()))
                .zipWith(contactRepository.insertContact(
                                        register.email(),
                                        Contact.ContactType.EMAIL
                                )
                )
                .flatMap(tuple2 -> contactRepository.saveUserContact(
                                        tuple2.getT1().id(),
                                        tuple2.getT2().id()
                                )
                                .then(Mono.just(tuple2.getT1()))
                )
                .switchIfEmpty(Mono.error(new IllegalArgumentException("must return creds")));
    }

    @Override
    public Mono<UserDetails> updatePassword(UserDetails user, String newPassword) {
        return null;
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return null;
    }
}
