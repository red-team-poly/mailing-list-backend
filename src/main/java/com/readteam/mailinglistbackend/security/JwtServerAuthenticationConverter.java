package com.readteam.mailinglistbackend.security;

import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.logging.Level;

@Component
public class JwtServerAuthenticationConverter implements ServerAuthenticationConverter {

    private final JwtHelper helper;

    public JwtServerAuthenticationConverter(JwtHelper helper) {
        this.helper = helper;
    }

    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        return Mono.justOrEmpty(exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
                .map(header -> header.split(" ")[1])
                .map(jwt -> new BearerToken(jwt, List.of(helper.getAuthority(jwt))));
    }
}

