package com.readteam.mailinglistbackend.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

public class BearerToken extends AbstractAuthenticationToken {

    public final String token;

    public BearerToken(String token, List<String> authorities) {
        super(AuthorityUtils.createAuthorityList(authorities.toArray(new String[0])));
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }
}
