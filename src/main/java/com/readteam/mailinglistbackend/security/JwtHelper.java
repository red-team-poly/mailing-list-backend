package com.readteam.mailinglistbackend.security;

import com.readteam.mailinglistbackend.repositories.models.Credentials;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class JwtHelper {
    private final SecretKey key = Keys.hmacShaKeyFor("tNO+KhVrTj3B4q0+SEwz/NSvZq7y577jOjvY4uPgAR4=".getBytes());
    private final JwtParser parser = Jwts.parserBuilder().setSigningKey(key).build();

    public BearerToken generate(Credentials credentials) {
        var token = Jwts.builder()
                .setClaims(
                        Map.of(
                                "id", credentials.id(),
                                "authority", credentials.userRole().name())
                )
                .setSubject(credentials.username())
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(2, ChronoUnit.HOURS)))
                .signWith(key)
                .compact();
        return new BearerToken(token, List.of(credentials.userRole().name()));
    }

    public Long getId(BearerToken token) {
        return parser.parseClaimsJws(token.token).getBody().get("id", Long.class);
    }

    public String getAuthority(String jwt) {
        Claims claims;
        try {
            claims = parser.parseClaimsJws(jwt).getBody();
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }
        return claims.get("authority", String.class);
    }

    public boolean isValid(BearerToken token, Credentials credentials) {
        var claims = parser.parseClaimsJws(token.token).getBody();
        var expirationDate = claims.getExpiration();

        return expirationDate.after(Date.from(Instant.now()))
                && Objects.equals(claims.getSubject(), credentials.username())
                && Objects.equals(claims.get("id", Long.class), credentials.id())
                && Objects.equals(claims.get("authority", String.class), credentials.userRole().name());
    }

}
