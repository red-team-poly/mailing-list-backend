package com.readteam.mailinglistbackend.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.logging.Level;

@Component
public class JwtAuthenticationManager implements ReactiveAuthenticationManager {

    private final JwtHelper jwtHelper;
    private final CredentialsService credentialsService;

    public JwtAuthenticationManager(JwtHelper jwtHelper, CredentialsService credentialsService) {
        this.jwtHelper = jwtHelper;
        this.credentialsService = credentialsService;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) throws AuthenticationException {
        return Mono.just(authentication)
                .filter(auth -> auth instanceof BearerToken)
                .cast(BearerToken.class)
                .flatMap(this::validate)
                .onErrorMap(error -> new AuthenticationServiceException(error.getMessage()));
    }

    private Mono<Authentication> validate(BearerToken token) {
        return Mono.just(jwtHelper.getId(token))
                .flatMap(credentialsService::findById)
                .map(user -> {
                            if (jwtHelper.isValid(token, user.credentials())) {
                                return new UsernamePasswordAuthenticationToken(
                                        user.credentials().id(),
                                        user,
                                        user.credentials().getAuthorities()
                                );
                            }
                            throw new IllegalArgumentException("Token is not valid.");
                        }
                );
    }
}
