package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.HighSchool;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface SchoolRepository extends ReactiveCrudRepository<HighSchool, Long> {

    @Query("""
            SELECT
             high_school.id,
             high_school.name,
             i.name as institute_name
            FROM high_school
             INNER JOIN institute i on i.id = high_school.institute_id
            """)
    Flux<HighSchool> getSchools();

    @Query("""
            SELECT high_school_id FROM user_high_school
            WHERE user_id = :userId
            """)
    Flux<Integer> getHighSchoolsByUser(Long userId);

    @Query("""
            INSERT INTO user_high_school(user_id, high_school_id)
            SELECT $1, high_school.id
            FROM high_school
            WHERE high_school.id = ANY($2)
            RETURNING high_school_id
            """)
    Flux<Integer> assignSchools(Long userId, Integer[] highSchools);

    @Query("""
            DELETE FROM user_high_school
            WHERE user_id = $1
            RETURNING high_school_id
            """)
    Flux<Integer> removeSchools(Long userId);

    @Query("""
            SELECT
                   high_school.id,
                   high_school.name,
                   i.name as institute_name
            FROM high_school
             INNER JOIN institute i on i.id = high_school.institute_id
             INNER JOIN user_high_school uhs on high_school.id = uhs.high_school_id
            WHERE uhs.user_id = :userId
            """)
    Flux<HighSchool> findSchoolsByUserId(Long userId);

}
