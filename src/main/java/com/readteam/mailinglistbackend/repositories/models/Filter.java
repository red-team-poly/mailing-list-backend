package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

public record Filter(
        @Id Long id,
        String title,
        Boolean isPublic,
        Long creatorId,
        String expression,
        String humanReadableId
) implements Authored {
    @Override
    public Long author() {
        return creatorId;
    }

    @Override
    public Boolean getPublic() {
        return isPublic;
    }
}
