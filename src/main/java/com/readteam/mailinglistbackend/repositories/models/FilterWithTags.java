package com.readteam.mailinglistbackend.repositories.models;

import java.util.List;

public record FilterWithTags(
        Filter filter,
        List<Tag> tags
) {

}
