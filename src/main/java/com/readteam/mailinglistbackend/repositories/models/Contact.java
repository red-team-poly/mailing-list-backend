package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

public record Contact(
        @Id Long id,
        String address,
        ContactType type
) {
    public enum ContactType {EMAIL}
}
