package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public record Credentials(
        @Id Long id,
        String username,
        String email,
        String password,
        UserRole userRole,
        Boolean permission
) implements UserDetails {

    public enum UserRole {USER, ADMIN}

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(userRole.name());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static String concatName(String firstname, String secondname, String patronymic) {
        return "%s %s %s".formatted(firstname, secondname, patronymic == null ? "" : patronymic);
    }

    public boolean isAdmin() {
        return userRole == UserRole.ADMIN;
    }
}
