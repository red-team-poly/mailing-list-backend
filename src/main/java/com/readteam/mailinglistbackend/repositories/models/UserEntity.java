package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

public record UserEntity(
        @Id Long id,
        String firstname,
        String secondname,
        String patronymic,
        Boolean permission
) {
}
