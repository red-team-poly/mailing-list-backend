package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

public record HighSchool(
        @Id Long id,
        String name,
        String instituteName
) {
}
