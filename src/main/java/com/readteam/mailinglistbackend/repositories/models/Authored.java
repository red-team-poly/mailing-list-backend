package com.readteam.mailinglistbackend.repositories.models;

import com.readteam.mailinglistbackend.model.PublicPermissions;

public interface Authored extends PublicPermissions {
    Long author();
}
