package com.readteam.mailinglistbackend.repositories.models;

import java.util.List;

public record CredentialsWithSchools(
        Credentials credentials,
        List<Integer> schoolIds
) {
}
