package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

import java.util.Map;

public record Student(
        @Id Long id,
        String firstname,
        String secondname,
        String patronymic,
        Map<String, String> attributes
) {
}
