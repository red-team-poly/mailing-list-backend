package com.readteam.mailinglistbackend.repositories.models;

import org.springframework.data.annotation.Id;

public record Attribute(
        @Id Long id,
        String value,
        String title
) {
}
