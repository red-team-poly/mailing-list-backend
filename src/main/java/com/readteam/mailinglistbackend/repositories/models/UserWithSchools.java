package com.readteam.mailinglistbackend.repositories.models;

import java.util.List;

public record UserWithSchools(
        UserEntity user,
        List<HighSchool> highSchools
) {
}
