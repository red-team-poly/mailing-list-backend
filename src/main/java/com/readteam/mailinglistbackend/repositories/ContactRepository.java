package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Contact;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ContactRepository extends ReactiveCrudRepository<Contact, Long> {

    @Query("""
            INSERT INTO user_contact(contact_id, user_id) VALUES (:contactId, :userId)
            """)
    Mono<Void> saveUserContact(Long userId, Long contactId);

    @Query("""
            INSERT INTO contact(address, type) VALUES (:address, :type::contact_type)
            RETURNING id, address::text, type::text
            """)
    Mono<Contact> insertContact(String address, Contact.ContactType type);
}
