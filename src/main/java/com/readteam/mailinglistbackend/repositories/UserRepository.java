package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.UserEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCrudRepository<UserEntity, Long> {

    @Query("""
            SELECT id, firstname, secondname, patronymic, permission FROM user_entity
            WHERE (
                  CASE
                      WHEN (:name <> '')
                          THEN (to_tsvector(firstname || ' ' || secondname || ' ' || (coalesce(patronymic, ''))) @@
                                to_tsquery('simple', :name ||':*'))
                      ELSE
                          TRUE
                      END
                  )
            LIMIT :limit
            OFFSET :offset
            """
    )
    Flux<UserEntity> getUsers(String name, int limit, int offset);

    @Query("""
            UPDATE user_entity
            SET permission = :permission
            WHERE id = :id
            RETURNING id, firstname, secondname, patronymic, permission
            """)
    Mono<UserEntity> updatePermissions(Long id, Boolean permission);
}
