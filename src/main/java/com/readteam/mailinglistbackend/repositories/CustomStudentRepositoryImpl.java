package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.converters.JsonToMapConverter;
import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.Student;
import io.r2dbc.postgresql.codec.Json;
import io.r2dbc.spi.Row;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public class CustomStudentRepositoryImpl implements CustomStudentRepository {

    private final DatabaseClient client;
    private JsonToMapConverter jsonToMapConverter;

    public CustomStudentRepositoryImpl(DatabaseClient client, JsonToMapConverter jsonToMapConverter) {
        this.jsonToMapConverter = jsonToMapConverter;
        this.client = client;
    }

    @Override
    public Flux<Student> findAllByExpression(String expression, Integer[] highSchools, int limit, int offset) {
        var query = """
                SELECT
                    student.id,
                    firstname,
                    secondname,
                    patronymic,
                    json_object_agg(a.title, av.value) as attributes
                FROM student
                         INNER JOIN student_tag st on student.id = st.student_id
                         INNER JOIN student_high_school shs on student.id = shs.student_id
                         INNER JOIN student_attribute_value sav on student.id = sav.student_id
                         INNER JOIN attribute_value av on av.id = sav.attribute_value_id
                         INNER JOIN attribute a on av.attribute_id = a.id
                WHERE high_school_id = ANY($1)
                GROUP BY student.id
                HAVING (%s)
                LIMIT $2
                OFFSET $3
                """
                .formatted(expression);
        return client.sql(query)
                .bind(0, highSchools)
                .bind(1, limit)
                .bind(2, offset)
                .map(studentMapper)
                .all();
    }

    @Override
    public Flux<Contact> findContactsByExpression(
            String expression,
            Integer[] highSchools,
            Contact.ContactType contactType) {
        var query = """
                SELECT id, address::text, type::text
                FROM contact c
                         INNER JOIN student_contact sc
                         INNER JOIN student_tag st on sc.student_id = st.student_id
                         INNER JOIN student_high_school shs on sc.student_id = shs.student_id on c.id = sc.contact_id
                WHERE high_school_id = ANY($1)
                  AND c.type = $2::contact_type
                GROUP BY c.id
                HAVING %s
                """
                .formatted(expression);
        return client.sql(query)
                .bind(0, highSchools)
                .bind(1, contactType.name())
                .map(contactMapper)
                .all();
    }

    private final Function<Row, Student> studentMapper = (row -> new Student(
            row.get("id", Long.class),
            row.get("firstname", String.class),
            row.get("secondname", String.class),
            row.get("patronymic", String.class),
            jsonToMapConverter.convert(row.get("attributes", Json.class))
    ));

    private final Function<Row, Contact> contactMapper = (row -> new Contact(
            row.get("id", Long.class),
            row.get("address", String.class),
            Contact.ContactType.valueOf(row.get("type", String.class))
    ));

}
