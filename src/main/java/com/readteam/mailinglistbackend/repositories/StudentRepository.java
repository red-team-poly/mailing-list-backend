package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Student;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface StudentRepository extends ReactiveCrudRepository<Student, Long>, CustomStudentRepository {

    @Query("""
            SELECT student.id,
                   firstname,
                   secondname,
                   patronymic,
                   json_object_agg(a.title, av.value) as attributes
            FROM student
                 LEFT JOIN student_tag st on student.id = st.student_id
                 INNER JOIN student_high_school shs on student.id = shs.student_id
                 INNER JOIN student_attribute_value sav on student.id = sav.student_id
                 INNER JOIN attribute_value av on av.id = sav.attribute_value_id
                 INNER JOIN attribute a on av.attribute_id = a.id
            WHERE high_school_id = ANY($3)
            AND (
                      CASE
                          WHEN ($4 <> '')
                              THEN (to_tsvector(firstname || ' ' || secondname || ' ' || (coalesce(patronymic, ''))) @@
                                    to_tsquery('simple', $4 ||':*'))
                          ELSE
                              TRUE
                          END
                      )
            GROUP BY student.id
            HAVING array_agg(sav.attribute_value_id) @> $1
               AND array_agg(st.tag_id) @> $2
            LIMIT $5
            OFFSET $6
            """)
    Flux<Student> getStudentsByTagsAndAttributes(
            Integer[] attributes,
            Integer[] tags,
            Integer[] highSchools,
            String name,
            int limit,
            int offset
    );


}
