package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Filter;
import org.springframework.data.annotation.Id;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface FilterRepository extends ReactiveCrudRepository<Filter, Long> {


    @Query("""
            SELECT id, title, is_public, creator_id, expression, human_readable_id
            FROM filter
            WHERE (CASE WHEN (:creatorId IS NOT NULL) THEN filter.creator_id = :creatorId ELSE TRUE END)
              AND (filter.is_public = :isPublic)
              AND (
                   CASE WHEN (:search <> '') THEN
                        (to_tsvector(title || ' ' || human_readable_id) @@  to_tsquery('simple', :search ||':*'))
                   ELSE TRUE END
                  )
            LIMIT :limit
            OFFSET :offset
            """)
    Flux<Filter> getFilters(
            Long creatorId,
            Boolean isPublic,
            String search,
            int limit,
            int offset
    );

    Mono<Boolean> existsByTitleAndIsPublicAndIdNot(String title, Boolean isPublic, Long id);

    Mono<Boolean> existsByHumanReadableId(String humanReadableId);

    @Query("""
            UPDATE filter
            SET title = :title,
                is_public = :isPublic,
                expression = :expression
            WHERE id = :id
            RETURNING id, title, is_public, creator_id, expression, human_readable_id
            """)
    Mono<Filter> updateFilter(Long id,
                              String title,
                              Boolean isPublic,
                              String expression);

    Mono<Filter> findByHumanReadableId(String id);

    @Query(
            """
                            SELECT id, title, is_public, creator_id, expression, human_readable_id
                            FROM filter
                            WHERE id = :id AND (creator_id =:creatorId OR is_public = TRUE)
                    """
    )
    Mono<Filter> findByIdAndCreatorIdOrIsPublic(Long id, Long creatorId);

}
