package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.converters.JsonToMapConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.dialect.PostgresDialect;

import java.util.List;

@Configuration
public class R2dbcConfig {

    @Bean
    public JsonToMapConverter jsonToMapConverter(){
        return new JsonToMapConverter();
    }

    @Bean
    public R2dbcCustomConversions customConversions(JsonToMapConverter jsonToMapConverter) {
        return R2dbcCustomConversions.of(PostgresDialect.INSTANCE, List.of(jsonToMapConverter));
    }
}
