package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Tag;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TagRepository extends ReactiveCrudRepository<Tag, Long> {

    @Query("""
            SELECT tag.id, tag.title, tag.is_public, tag.creator_id
            FROM tag
            WHERE (CASE WHEN (:creatorId IS NOT NULL) THEN tag.creator_id = :creatorId ELSE TRUE END)
              AND (tag.is_public = :isPublic)
              AND (CASE WHEN (:title <> '') THEN (title LIKE '%'||:title||'%') ELSE TRUE END)
            LIMIT :limit
            OFFSET :offset
            """)
    Flux<Tag> getTags(
            Long creatorId,
            Boolean isPublic,
            String title,
            int limit,
            int offset
    );

    @Query("""
            INSERT INTO student_tag(student_id, tag_id)
            SELECT student_id, $2
            FROM student_high_school shs
            WHERE shs.high_school_id = ANY($3)
             AND shs.student_id = ANY($1)
            RETURNING student_id
            """)
    Flux<Integer> assignTags(Integer[] studentIds, Long tagId, Integer[] highSchools);

    @Query("""
            DELETE FROM student_tag
            WHERE tag_id = $2 AND student_id IN (
                SELECT student_id
                FROM student_high_school shs
                WHERE shs.high_school_id = ANY($1)
            )
            RETURNING student_id
            """)
    Flux<Integer> removeTags(Integer[] highSchools, Long tagId);

    Mono<Boolean> existsByTitleAndIsPublicAndIdNot(String title, Boolean isPublic, Long tagId);

    Flux<Tag> findByCreatorIdOrIsPublic(Long creatorId, Boolean isPublic);

    @Query("""
            SELECT tag.id, tag.title, tag.is_public, tag.creator_id
            FROM tag INNER JOIN tag_filter tf on tag.id = tf.tag_id
            WHERE filter_id = :filterId
            """)
    Flux<Tag> findByFilterId(Long filterId);

    @Query("""
            SELECT tag.id
            FROM tag
            WHERE id = ANY($1) AND (creator_id =$2 OR is_public = TRUE)
            """)
    Flux<Long> findAllByIdsAndCreatorId(Long[] ids, Long creatorId);

    @Query("""
            DELETE FROM tag_filter
            WHERE filter_id = :id
            RETURNING tag_id
            """)
    Flux<Long> removeTagsByFilterId(Long id);

    @Query("""
            INSERT INTO tag_filter(filter_id, tag_id)
            VALUES ($1, unnest($2))
            RETURNING tag_id
            """)
    Flux<Long> addTagsToFilter(Long id, Long[] ids);

}
