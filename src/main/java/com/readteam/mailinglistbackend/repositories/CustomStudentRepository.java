package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.Student;
import reactor.core.publisher.Flux;

public interface CustomStudentRepository {
    Flux<Student> findAllByExpression(String expression, Integer[] highSchools, int limit, int offset);

    Flux<Contact> findContactsByExpression(String expression, Integer[] highSchools, Contact.ContactType contactType);
}
