package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.Credentials;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface AuthRepository extends ReactiveCrudRepository<Credentials, Long> {

    @Query("""
            SELECT id,
             firstname ||' '||secondname || ' ' || coalesce(patronymic, '') as username,
             email,
             password,
             user_role::text,
             permission
            FROM credentials INNER JOIN user_entity ue on ue.id = credentials.user_id
                WHERE email = :email
            """)
    Mono<Credentials> getUserByEmail(String email);

    @Query("""
            SELECT id,
             firstname ||' '||secondname || ' ' || coalesce(patronymic, '') as username,
             email,
             password,
             user_role::text,
             permission
            FROM credentials INNER JOIN user_entity ue on ue.id = credentials.user_id
                WHERE id = :id
            """)
    Mono<Credentials> getUserById(Long id);

    @Query("""
            INSERT INTO credentials(user_id, email, password)
            VALUES (:userId, :email, :password)
            RETURNING user_id as id, email, password, user_role::text
            """)
    Mono<Credentials> insertCredentials(Long userId, String email, String password);

    @Query("""
            SELECT user_id
            FROM  user_contact uc
            INNER JOIN contact c on c.id = uc.contact_id
            WHERE c.address = :address AND c.type = :type::contact_type
            """)
    Mono<Long> findUserIdByContact(String address, Contact.ContactType type);


}
