package com.readteam.mailinglistbackend.repositories;

import com.readteam.mailinglistbackend.repositories.models.Attribute;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface AttributesRepository extends ReactiveCrudRepository<Attribute, Long> {

    @Query("""
            SELECT attribute_value.id,
                   value,
                   title
            FROM attribute_value
                INNER JOIN attribute a on a.id = attribute_value.attribute_id
            """)
    Flux<Attribute> getAttributes();

}
