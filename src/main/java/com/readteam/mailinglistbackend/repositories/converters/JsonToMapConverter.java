package com.readteam.mailinglistbackend.repositories.converters;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.r2dbc.postgresql.codec.Json;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@ReadingConverter
public class JsonToMapConverter implements Converter<Json, Map<String, String>> {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Map<String, String> convert(Json json) {
        try {
            return objectMapper.readValue(json.asString(), new TypeReference<>() {
            });
        } catch (IOException e) {
            Logger.getLogger("JsonToMapConverter").log(Level.SEVERE, "Problem while parsing JSON: {}" + json, e);
        }
        return new HashMap<>();
    }
}
