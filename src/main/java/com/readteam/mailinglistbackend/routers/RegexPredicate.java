package com.readteam.mailinglistbackend.routers;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class RegexPredicate implements Predicate<String> {

    private final Pattern pattern;

    public RegexPredicate(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public boolean test(String s) {
        return pattern.matcher(s).matches();
    }
}
