package com.readteam.mailinglistbackend.routers;

import com.readteam.mailinglistbackend.handlers.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.*;

@Component
public class MainRouter {

    @Bean
    public RouterFunction<ServerResponse> mainRoute(
            StudentHandler studentHandler,
            TagHandler tagHandler,
            UserHandler userHandler,
            AuthHandler authHandler,
            FilterHandler filterHandler
    ) {
        return RouterFunctions
                .route()
                .GET("/attributes", tagHandler::getAttributes)
                .GET("/schools", userHandler::getSchools)
                .GET("/students", studentHandler::getStudents)
                .GET("/students-by-filter/{id:[0-9]+}", studentHandler::getStudentsByFilter)
                .GET("/contacts-by-filter/{filter:[0-9a-z\\-]+}", studentHandler::getContactsByFilter)
                .GET(
                        "/students-by-expression",
                        RequestPredicates.queryParam("exp", new RegexPredicate("[0-9!&|()]+")),
                        studentHandler::getStudentByExpression
                )
                .path("/users", builder -> builder
                        .path("/{id:[0-9]+}", subBuilder -> subBuilder
                                .PUT(userHandler::updateUser)
                                .GET(userHandler::getUser)
                        )
                        .GET(userHandler::getUsers)
                )
                .path("/tags", builder -> builder
                        .path("/{id:[0-9]+}", subBuilder -> subBuilder
                                .PUT(tagHandler::updateTag)
                                .DELETE(tagHandler::deleteTag)
                        )
                        .GET(tagHandler::getTags)
                        .POST(tagHandler::createTag)
                )
                .path("/filters", builder -> builder
                        .path("/{id:[0-9]+}", subBuilder -> subBuilder
                                .PUT(filterHandler::updateFilter)
                                .DELETE(filterHandler::deleteFilter)
                                .GET(filterHandler::getFilter)
                        )
                        .GET(filterHandler::getFilters)
                        .POST(filterHandler::createFilter)
                )
                .POST("/login", authHandler::login)
                .POST("/register", authHandler::register)
                .build();
    }
}
