package com.readteam.mailinglistbackend.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class Errors {
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "resource not found")
    static public class NotFoundException extends RuntimeException {
        public NotFoundException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "empty body")
    static public class EmptyBodyException extends RuntimeException {
        public EmptyBodyException() {
            super("empty body");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "already exists")
    static public class AlreadyExistsException extends RuntimeException {
        public AlreadyExistsException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "can't create public")
    static public class PublicPermissionsException extends RuntimeException {
        public PublicPermissionsException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "can't change public entity to private")
    static public class ChangePublicPermissionsException extends RuntimeException {
        public ChangePublicPermissionsException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "can't access resource, wrong owner")
    static public class UnauthorizedResourceAccessException extends RuntimeException {
        public UnauthorizedResourceAccessException() {
            super("");
        }
    }


    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "bad path variable")
    static public class PathVariableError extends RuntimeException {
        public PathVariableError() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "bad query variable")
    static public class QueryVariableException extends RuntimeException {
        public QueryVariableException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "bad body")
    static public class ValidationError extends RuntimeException {
        public ValidationError() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "violates unique constraint for public item")
    static public class UniqueConstraintException extends RuntimeException {
        public UniqueConstraintException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "public item deletion")
    static public class PublicItemDeletionException extends RuntimeException {
        public PublicItemDeletionException() {
            super("");
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    static public class BadExpressionException extends RuntimeException {
        public BadExpressionException(String reason) {
            super(reason);
        }
    }

}
