package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.model.UserRequest;
import com.readteam.mailinglistbackend.repositories.models.UserEntity;
import com.readteam.mailinglistbackend.services.SchoolService;
import com.readteam.mailinglistbackend.services.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class UserHandler extends HandlerUtils.BasicHandler {

    private final SchoolService schoolService;
    private final UserService userService;

    public UserHandler(SchoolService schoolService, UserService userService) {
        this.schoolService = schoolService;
        this.userService = userService;
    }

    public Mono<ServerResponse> getSchools(ServerRequest serverRequest) {
        return schoolService.getSchools().flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> getUsers(ServerRequest serverRequest) {
        var name = HandlerUtils.getSearch(serverRequest);
        return ServerResponse.ok().body(
                userService.getUsers(
                        name,
                        HandlerUtils.getPageable(serverRequest)
                ),
                UserEntity.class
        );
    }

    public Mono<ServerResponse> updateUser(ServerRequest serverRequest) {
        var id = HandlerUtils.getIdPathVariable(serverRequest);
        return getBody(serverRequest, UserRequest.class)
                .flatMap(userRequest -> userService.updateUser(id.longValue(), userRequest))
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> getUser(ServerRequest serverRequest) {
        var id = HandlerUtils.getIdPathVariable(serverRequest);
        return userService.getUserWithSchools(id.longValue()).flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }
}
