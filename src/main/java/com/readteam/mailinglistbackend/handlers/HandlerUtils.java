package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.CredentialsWithSchools;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.springframework.web.reactive.function.server.ServerResponse.notFound;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

public class HandlerUtils {
    final static String PAGE_KEY = "page";
    final static String PAGE_SIZE_KEY = "size";
    final static String SEARCH_KEY = "search";
    final static String ID_PATH = "id";
    final static String CREATED_BY_USER_KEY = "by-user";
    final static String IS_PUBLIC_KEY = "public";
    final static int PAGE_SIZE = 50;

    @Component
    static class BasicHandler {
        private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


        private <T> T validate(T data) {
            var errors = validator.validate(data);
            Logger.getGlobal().log(Level.SEVERE, errors.toString());
            if (!errors.isEmpty()) throw new Errors.ValidationError();
            return data;
        }

        protected <T> Mono<? extends T> getBody(ServerRequest serverRequest, Class<? extends T> elementClass) {
            return serverRequest.bodyToMono(elementClass)
                    .map(this::validate)
                    .switchIfEmpty(Mono.error(Errors.EmptyBodyException::new));
        }

    }

    public static List<Integer> getIntListQueryParams(ServerRequest serverRequest, String key) {
        return serverRequest
                .queryParams()
                .getOrDefault(key, Collections.emptyList())
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public static Optional<Integer> getIntQueryParam(ServerRequest serverRequest, String key) {
        return serverRequest
                .queryParam(key)
                .map(Integer::parseInt);
    }

    public static Optional<Boolean> getBooleanQueryParam(ServerRequest serverRequest, String key) {
        return serverRequest
                .queryParam(key)
                .map(Boolean::parseBoolean);
    }

    public static int getPage(ServerRequest serverRequest) {
        return getIntQueryParam(serverRequest, PAGE_KEY)
                .filter(page -> page >= 0)
                .orElse(0);
    }

    public static int getPageSize(ServerRequest serverRequest, int defaultPageSize) {
        return getIntQueryParam(serverRequest, PAGE_SIZE_KEY)
                .filter(page -> page > 0)
                .orElse(defaultPageSize);
    }

    public static int getPageSize(ServerRequest serverRequest) {
        return getPageSize(serverRequest, PAGE_SIZE);
    }

    public static Pageable getPageable(ServerRequest serverRequest) {
        return PageRequest.of(getPage(serverRequest), getPageSize(serverRequest));
    }

    public static String getSearch(ServerRequest serverRequest) {
        return serverRequest.queryParam(SEARCH_KEY).orElse("");
    }

    public static Mono<CredentialsWithSchools> getCredentials(ServerRequest serverRequest) {
        return serverRequest
                .principal()
                .cast(UsernamePasswordAuthenticationToken.class)
                .map(p -> ((CredentialsWithSchools) p.getCredentials()));
    }

    public static Integer getIdPathVariable(ServerRequest serverRequest) {
        return Optional.of(serverRequest.pathVariable(ID_PATH)).map(Integer::parseInt).orElseThrow(Errors.PathVariableError::new);
    }

    public static Boolean getCreatedByUserParam(ServerRequest serverRequest) {
        return getBooleanQueryParam(serverRequest, CREATED_BY_USER_KEY).orElse(true);
    }

    public static Boolean getIsPublicParam(ServerRequest serverRequest) {
        return getBooleanQueryParam(serverRequest, IS_PUBLIC_KEY).orElse(false);
    }

}
