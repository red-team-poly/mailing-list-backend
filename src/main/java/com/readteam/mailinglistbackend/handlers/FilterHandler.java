package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.model.FilterRequest;
import com.readteam.mailinglistbackend.model.FilterResponse;
import com.readteam.mailinglistbackend.repositories.models.Filter;
import com.readteam.mailinglistbackend.repositories.models.FilterWithTags;
import com.readteam.mailinglistbackend.services.FilterService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.logging.Level;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class FilterHandler extends HandlerUtils.BasicHandler {
    private final FilterService filterService;

    public FilterHandler(FilterService filterService) {
        this.filterService = filterService;
    }

    public Mono<ServerResponse> getFilters(ServerRequest serverRequest) {
        var createdByUser = HandlerUtils.getCreatedByUserParam(serverRequest);
        var isPublic = HandlerUtils.getIsPublicParam(serverRequest);
        var title = HandlerUtils.getSearch(serverRequest);
        return ServerResponse.ok().body(
                HandlerUtils.getCredentials(serverRequest)
                        .flatMapMany(credentialsWithSchools ->
                                filterService.getFilters(
                                        credentialsWithSchools.credentials(),
                                        createdByUser,
                                        isPublic,
                                        title,
                                        HandlerUtils.getPageable(serverRequest)
                                )
                        ),
                Filter.class
        );
    }

    public Mono<ServerResponse> updateFilter(ServerRequest serverRequest) {
        var filterId = HandlerUtils.getIdPathVariable(serverRequest);
        return insertOrUpdate(serverRequest, filterId.longValue())
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> createFilter(ServerRequest serverRequest) {
        return insertOrUpdate(serverRequest, null)
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    private Mono<FilterResponse> insertOrUpdate(ServerRequest serverRequest, Long filterId) {
        return getBody(serverRequest, FilterRequest.class)
                .zipWith(HandlerUtils.getCredentials(serverRequest))
                .flatMap(tuple2 -> filterService.createOrUpdateFilter(
                                tuple2.getT2().credentials(),
                                tuple2.getT1(),
                                filterId
                        )
                );
    }

    public Mono<ServerResponse> deleteFilter(ServerRequest serverRequest) {
        var filterId = HandlerUtils.getIdPathVariable(serverRequest);
        return HandlerUtils.getCredentials(serverRequest)
                .flatMap(c -> filterService.deleteFilter(c.credentials(), filterId.longValue()))
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> getFilter(ServerRequest serverRequest) {
        var filterId = HandlerUtils.getIdPathVariable(serverRequest);
        return HandlerUtils.getCredentials(serverRequest)
                .flatMap(c -> filterService.getFilter(c.credentials(), filterId.longValue()))
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }
}
