package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.Student;
import com.readteam.mailinglistbackend.services.StudentService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.logging.Logger;

@Component
public class StudentHandler {

    private final static String ATTRIBUTES_KEY = "attr";
    private final static String TAG_KEY = "tag";
    private final static String TYPE_KEY = "type";
    private final static String CONTACT_KEY = "address";
    private final static String FILTER_NAME = "filter";
    private final static String EXPRESSION_KEY = "exp";

    private final StudentService studentService;

    public StudentHandler(StudentService studentService) {
        this.studentService = studentService;
    }

    public Mono<ServerResponse> getStudents(ServerRequest serverRequest) {
        var attributes = HandlerUtils.getIntListQueryParams(serverRequest, ATTRIBUTES_KEY);
        var tags = HandlerUtils.getIntListQueryParams(serverRequest, TAG_KEY);
        var name = HandlerUtils.getSearch(serverRequest);
        return ServerResponse.ok().body(
                HandlerUtils.getCredentials(serverRequest)
                        .flatMapMany(credentialsWithSchools ->
                                studentService.getStudents(
                                        credentialsWithSchools,
                                        attributes,
                                        tags,
                                        name,
                                        HandlerUtils.getPageable(serverRequest)
                                )
                        ),
                Student.class);
    }

    public Mono<ServerResponse> getStudentsByFilter(ServerRequest serverRequest) {
        var filterId = HandlerUtils.getIdPathVariable(serverRequest);
        var pageable = HandlerUtils.getPageable(serverRequest);
        return ServerResponse.ok().body(
                HandlerUtils.getCredentials(serverRequest)
                        .flatMapMany(credentialsWithSchools ->
                                studentService.getStudentsByFilter(
                                        credentialsWithSchools,
                                        filterId.longValue(),
                                        pageable
                                )
                        ),
                Student.class
        );
    }

    public Mono<ServerResponse> getContactsByFilter(ServerRequest serverRequest) {
        var filterName = serverRequest.pathVariable(FILTER_NAME);
        var type = getContactType(serverRequest);
        var userAddress = serverRequest.queryParam(CONTACT_KEY).orElseThrow(Errors.QueryVariableException::new);
        return ServerResponse.ok().body(
                studentService.getContactsByFilter(
                        userAddress,
                        filterName,
                        type
                ),
                Contact.class
        );
    }

    private Contact.ContactType getContactType(ServerRequest serverRequest) {
        return serverRequest
                .queryParam(TYPE_KEY)
                .map(String::toUpperCase)
                .map(Contact.ContactType::valueOf)
                .orElse(Contact.ContactType.EMAIL);
    }

    public Mono<ServerResponse> getStudentByExpression(ServerRequest serverRequest) {
        var expression = serverRequest.queryParam(EXPRESSION_KEY).orElseThrow(Errors.QueryVariableException::new);
        var pageable = HandlerUtils.getPageable(serverRequest);
        return ServerResponse.ok().body(
                HandlerUtils.getCredentials(serverRequest)
                        .flatMapMany(credentialsWithSchools ->
                                studentService.getStudentsByExpression(
                                        credentialsWithSchools,
                                        expression,
                                        pageable
                                )
                        ),
                Student.class
        );
    }
}
