package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.model.TagRequest;
import com.readteam.mailinglistbackend.model.TagResponse;
import com.readteam.mailinglistbackend.repositories.models.Tag;
import com.readteam.mailinglistbackend.services.AttributesService;
import com.readteam.mailinglistbackend.services.TagService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.logging.Level;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class TagHandler extends HandlerUtils.BasicHandler {

    private final TagService tagService;
    private final AttributesService attributesService;


    public TagHandler(TagService tagService, AttributesService attributesService) {
        this.tagService = tagService;
        this.attributesService = attributesService;
    }

    public Mono<ServerResponse> getAttributes(ServerRequest serverRequest) {
        return attributesService
                .getAttributes()
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> getTags(ServerRequest serverRequest) {
        var createdByUser = HandlerUtils.getCreatedByUserParam(serverRequest);
        var isPublic = HandlerUtils.getIsPublicParam(serverRequest);
        var title = HandlerUtils.getSearch(serverRequest);
        return ServerResponse.ok().body(
                HandlerUtils.getCredentials(serverRequest)
                        .flatMapMany(credentialsWithSchools ->
                                tagService.getTags(
                                        credentialsWithSchools.credentials(),
                                        createdByUser,
                                        isPublic,
                                        title,
                                        HandlerUtils.getPageable(serverRequest)
                                )
                        )
                , Tag.class
        );
    }

    public Mono<ServerResponse> createTag(ServerRequest serverRequest) {
        return insertOrUpdate(serverRequest, null)
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> updateTag(ServerRequest serverRequest) {
        var tagId = HandlerUtils.getIdPathVariable(serverRequest);
        return insertOrUpdate(serverRequest, tagId.longValue())
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    private Mono<TagResponse> insertOrUpdate(ServerRequest serverRequest, Long tagId) {
        return getBody(serverRequest, TagRequest.class)
                .zipWith(HandlerUtils.getCredentials(serverRequest))
                .flatMap(tuple2 -> tagService.createOrUpdateTag(
                                tuple2.getT2(),
                                tuple2.getT1(),
                                tagId
                        )
                );
    }

    public Mono<ServerResponse> deleteTag(ServerRequest serverRequest) {
        var tagId = HandlerUtils.getIdPathVariable(serverRequest);
        return HandlerUtils.getCredentials(serverRequest)
                .flatMap(c -> tagService.deleteTag(c.credentials(), tagId.longValue()))
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }
}
