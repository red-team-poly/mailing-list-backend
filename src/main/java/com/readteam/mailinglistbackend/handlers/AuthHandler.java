package com.readteam.mailinglistbackend.handlers;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.model.AuthResponse;
import com.readteam.mailinglistbackend.model.Jwt;
import com.readteam.mailinglistbackend.model.Login;
import com.readteam.mailinglistbackend.model.Register;
import com.readteam.mailinglistbackend.security.JwtHelper;
import com.readteam.mailinglistbackend.security.CredentialsService;
import com.readteam.mailinglistbackend.services.UserService;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class AuthHandler extends HandlerUtils.BasicHandler {
    private final CredentialsService credentialsService;
    private final PasswordEncoder encoder;
    private final JwtHelper jwtHelper;
    private final UserService userService;

    public AuthHandler(
            CredentialsService credentialsService,
            PasswordEncoder encoder,
            JwtHelper jwtHelper,
            UserService userService
    ) {
        this.credentialsService = credentialsService;
        this.encoder = encoder;
        this.jwtHelper = jwtHelper;
        this.userService = userService;
    }

    public Mono<ServerResponse> login(ServerRequest serverRequest) {
        return getBody(serverRequest, Login.class)
                .flatMap(login -> credentialsService.findByEmail(login.email()).map(cred -> Pair.of(login, cred)))
                .switchIfEmpty(Mono.error(Errors.NotFoundException::new))
                .map(pair -> {
                    if (encoder.matches(pair.getFirst().password(), pair.getSecond().password())) {
                        return pair.getSecond();
                    } else throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
                })
                .flatMap(credentials -> userService.getUser(credentials.id())
                        .map(user -> Pair.of(credentials, user))
                )
                .map(pair -> new AuthResponse(
                                pair.getSecond(),
                                pair.getFirst().userRole(),
                                new Jwt(jwtHelper.generate(pair.getFirst()).token)
                        )
                )
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

    public Mono<ServerResponse> register(ServerRequest serverRequest) {
        return getBody(serverRequest, Register.class)
                .flatMap(register ->
                        credentialsService.findByEmail(register.email())
                                .flatMap(__ -> Mono.error(Errors.AlreadyExistsException::new))
                                .switchIfEmpty(Mono.just(register))
                                .cast(Register.class)
                )
                .flatMap(credentialsService::createUser)
                .flatMap(credentials -> userService.getUser(credentials.id())
                        .map(user -> Pair.of(credentials, user))
                )
                .map(pair -> new AuthResponse(
                                pair.getSecond(),
                                pair.getFirst().userRole(),
                                new Jwt(jwtHelper.generate(pair.getFirst()).token)
                        )
                )
                .flatMap(c -> ok().body(BodyInserters.fromValue(c)));
    }

}
