package com.readteam.mailinglistbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailingListBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailingListBackendApplication.class, args);
	}

}
