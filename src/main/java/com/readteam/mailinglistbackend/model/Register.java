package com.readteam.mailinglistbackend.model;

import javax.validation.constraints.NotNull;

public record Register(
        @NotNull
        String firstname,
        @NotNull
        String secondname,
        String patronymic,
        @NotNull
        String email,
        @NotNull
        String password
) {
}
