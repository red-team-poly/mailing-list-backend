package com.readteam.mailinglistbackend.model;

public record Jwt(String token) {

}
