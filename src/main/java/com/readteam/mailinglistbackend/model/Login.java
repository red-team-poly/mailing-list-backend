package com.readteam.mailinglistbackend.model;

import javax.validation.constraints.NotNull;

public record Login(
        @NotNull
        String email,
        @NotNull
        String password
) {
}
