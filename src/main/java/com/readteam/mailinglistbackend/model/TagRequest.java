package com.readteam.mailinglistbackend.model;

import javax.validation.constraints.NotNull;
import java.util.List;

public record TagRequest(
        @NotNull
        String title,
        @NotNull
        Boolean isPublic,
        @NotNull
        List<Integer> studentIds
) implements PublicPermissions {
    @Override
    public Boolean getPublic() {
        return isPublic;
    }
}
