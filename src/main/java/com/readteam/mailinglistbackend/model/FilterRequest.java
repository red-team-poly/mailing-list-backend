package com.readteam.mailinglistbackend.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Set;

public record FilterRequest(
        @NotNull
        String title,
        @NotNull
        Boolean isPublic,
        @NotNull
        @Pattern(regexp = "[0-9!&|()]+")
        String expression,
        @NotNull
        Set<Long> tags
) implements PublicPermissions {
    @Override
    public Boolean getPublic() {
        return isPublic;
    }
}
