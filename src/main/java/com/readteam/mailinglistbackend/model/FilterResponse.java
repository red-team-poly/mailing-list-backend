package com.readteam.mailinglistbackend.model;

import com.readteam.mailinglistbackend.repositories.models.Filter;

import java.util.List;

public record FilterResponse(
        Filter filter,
        List<Long> tags
) {

}
