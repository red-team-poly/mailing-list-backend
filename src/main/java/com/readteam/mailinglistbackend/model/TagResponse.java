package com.readteam.mailinglistbackend.model;

import com.readteam.mailinglistbackend.repositories.models.Tag;

import java.util.List;

public record TagResponse(
        Tag tag,
        List<Integer> studentIds
) {
}
