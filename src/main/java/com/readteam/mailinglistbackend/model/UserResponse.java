package com.readteam.mailinglistbackend.model;

import com.readteam.mailinglistbackend.repositories.models.UserEntity;

import java.util.List;

public record UserResponse(
        UserEntity user,
        List<Integer> schools
) {
}
