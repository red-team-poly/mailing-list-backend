package com.readteam.mailinglistbackend.model;

import com.readteam.mailinglistbackend.repositories.models.Credentials;
import com.readteam.mailinglistbackend.repositories.models.UserEntity;

public record AuthResponse(UserEntity user, Credentials.UserRole userRole, Jwt token) {
}
