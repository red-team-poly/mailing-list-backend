package com.readteam.mailinglistbackend.model;


import javax.validation.constraints.NotNull;
import java.util.List;

public record UserRequest(
        @NotNull
        Boolean permission,
        @NotNull
        List<Integer> schoolIds
) {
}
