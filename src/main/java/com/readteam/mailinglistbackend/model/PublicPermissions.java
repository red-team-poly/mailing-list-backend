package com.readteam.mailinglistbackend.model;

public interface PublicPermissions {
    Boolean getPublic();
}
