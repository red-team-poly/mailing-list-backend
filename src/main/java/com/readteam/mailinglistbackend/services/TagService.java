package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.model.TagRequest;
import com.readteam.mailinglistbackend.model.TagResponse;
import com.readteam.mailinglistbackend.repositories.TagRepository;
import com.readteam.mailinglistbackend.repositories.models.Credentials;
import com.readteam.mailinglistbackend.repositories.models.CredentialsWithSchools;
import com.readteam.mailinglistbackend.repositories.models.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class TagService {

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public Flux<Tag> getTags(
            Credentials credentials,
            Boolean createdByUser,
            Boolean isPublic,
            String title,
            Pageable pageable
    ) {
        return tagRepository.getTags(
                createdByUser ? credentials.id() : null,
                createdByUser || credentials.isAdmin() ? isPublic : false,
                title,
                pageable.getPageSize(),
                ServiceUtils.getOffset(pageable)
        );
    }

    @Transactional
    public Mono<TagResponse> createOrUpdateTag(
            CredentialsWithSchools credentials,
            TagRequest tagRequest,
            Long tagId
    ) {
        return ServiceUtils.updateWithAuthorAndUnique(
                        credentials.credentials(),
                        tagRequest,
                        tagId,
                        () -> tagRepository.existsByTitleAndIsPublicAndIdNot(tagRequest.title(), tagRequest.isPublic(), tagId == null ? -1L : tagId),
                        tagRepository::findById,
                        this::updateOrCreate
                )
                .flatMap(e -> tagRepository.removeTags(credentials.schoolIds().toArray(new Integer[0]), e.id()).collectList().map(i -> e))
                .flatMap(tag -> tagRepository.assignTags(
                                        tagRequest.studentIds().toArray(new Integer[0]),
                                        tag.id(),
                                        credentials.schoolIds().toArray(new Integer[0])
                                )
                                .collectList()
                                .map(u -> new TagResponse(tag, u)
                                )
                );
    }

    private Mono<Tag> updateOrCreate(Long id, TagRequest tagRequest, Long authorId) {
        return tagRepository.save(
                new Tag(
                        id,
                        tagRequest.title(),
                        tagRequest.isPublic(),
                        authorId
                )
        );
    }

    public Mono<Tag> deleteTag(
            Credentials credentials,
            Long tagId
    ) {
        return ServiceUtils.deleteAuthored(
                credentials,
                tagId,
                tagRepository::findById,
                tagRepository::delete
        );
    }

}
