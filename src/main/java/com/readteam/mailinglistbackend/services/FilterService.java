package com.readteam.mailinglistbackend.services;

import com.github.kkuegler.PermutationBasedHumanReadableIdGenerator;
import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.model.FilterRequest;
import com.readteam.mailinglistbackend.model.FilterResponse;
import com.readteam.mailinglistbackend.repositories.FilterRepository;
import com.readteam.mailinglistbackend.repositories.TagRepository;
import com.readteam.mailinglistbackend.repositories.models.Credentials;
import com.readteam.mailinglistbackend.repositories.models.Filter;
import com.readteam.mailinglistbackend.repositories.models.FilterWithTags;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FilterService {

    private final PermutationBasedHumanReadableIdGenerator idGenerator = new PermutationBasedHumanReadableIdGenerator();
    private final FilterRepository filterRepository;
    private final FilterValidator filterValidator;
    private final TagRepository tagRepository;

    public FilterService(FilterRepository filterRepository, FilterValidator filterValidator, TagRepository tagRepository) {
        this.filterRepository = filterRepository;
        this.filterValidator = filterValidator;
        this.tagRepository = tagRepository;
    }

    public Flux<Filter> getFilters(
            Credentials credentials,
            Boolean createdByUser,
            Boolean isPublic,
            String search,
            Pageable pageable
    ) {
        return filterRepository.getFilters(
                createdByUser ? credentials.id() : null,
                createdByUser || credentials.isAdmin() ? isPublic : false,
                search,
                pageable.getPageSize(),
                ServiceUtils.getOffset(pageable)
        );
    }

    @Transactional
    public Mono<FilterResponse> createOrUpdateFilter(
            Credentials credentials,
            FilterRequest filterRequest,
            Long filterId
    ) {
        return ServiceUtils.updateWithAuthorAndUnique(
                        credentials,
                        filterRequest,
                        filterId,
                        () -> filterRepository.existsByTitleAndIsPublicAndIdNot(filterRequest.title(), filterRequest.isPublic(), filterId == null ? -1L : filterId),
                        filterRepository::findById,
                        this::updateOrCreate,
                        () -> checkLogic(credentials, filterRequest)
                )
                .flatMap(filter -> tagRepository.removeTagsByFilterId(filter.id()).collectList().map(ids -> filter))
                .flatMap(filter ->
                        tagRepository.addTagsToFilter(filter.id(), filterRequest.tags().toArray(Long[]::new))
                                .collectList()
                                .map(ids -> new FilterResponse(filter, ids))
                );
    }

    private Mono<Boolean> checkLogic(Credentials credentials, FilterRequest request) {
        return filterValidator.checkTagsInExpression(credentials, request)
                .then(Mono.just(filterValidator.checkSyntax(request.expression())))
                .then(Mono.just(true));
    }

    private Mono<Filter> updateOrCreate(Long id, FilterRequest request, Long authorId) {
        if (id == null) {
            return createFilter(request, authorId);
        } else {
            return filterRepository.updateFilter(
                    id,
                    request.title(),
                    request.isPublic(),
                    request.expression()
            );
        }
    }

    private Mono<String> createHumanReadableId() {
        var newId = idGenerator.generate();
        return filterRepository.existsByHumanReadableId(newId)
                .flatMap(t -> t ? createHumanReadableId() : Mono.just(newId));
    }

    private Mono<Filter> createFilter(FilterRequest request, Long authorId) {
        return createHumanReadableId().flatMap(id -> filterRepository.save(
                        new Filter(
                                null,
                                request.title(),
                                request.isPublic(),
                                authorId,
                                request.expression(),
                                id
                        )
                )
        );
    }

    public Mono<Filter> deleteFilter(
            Credentials credentials,
            Long filterId
    ) {
        return ServiceUtils.deleteAuthored(
                credentials,
                filterId,
                filterRepository::findById,
                filterRepository::delete
        );
    }

    public Mono<FilterWithTags> getFilter(
            Credentials credentials,
            Long filterId
    ) {
        return filterRepository.findByIdAndCreatorIdOrIsPublic(filterId, credentials.id())
                .zipWith(tagRepository.findByFilterId(filterId).collectList())
                .map(t -> new FilterWithTags(t.getT1(), t.getT2()))
                .switchIfEmpty(Mono.error(Errors.NotFoundException::new));
    }

}
