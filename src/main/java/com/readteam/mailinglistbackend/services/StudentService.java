package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.repositories.*;
import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.CredentialsWithSchools;
import com.readteam.mailinglistbackend.repositories.models.HighSchool;
import com.readteam.mailinglistbackend.repositories.models.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;

@Service
public class StudentService {

    private final StudentRepository studentRepository;
    private final FilterRepository filterRepository;
    private final FilterValidator filterValidator;
    private final AuthRepository authRepository;
    private final SchoolRepository schoolRepository;
    private final TagRepository tagRepository;

    public StudentService(StudentRepository studentRepository, FilterRepository filterRepository, FilterValidator filterValidator, AuthRepository authRepository, SchoolRepository schoolRepository, TagRepository tagRepository) {
        this.studentRepository = studentRepository;
        this.filterRepository = filterRepository;
        this.filterValidator = filterValidator;
        this.authRepository = authRepository;
        this.schoolRepository = schoolRepository;
        this.tagRepository = tagRepository;
    }

    public Flux<Student> getStudents(
            CredentialsWithSchools credentialsWithSchools,
            List<Integer> attributes,
            List<Integer> tags,
            String name,
            Pageable pageable
    ) {
        return studentRepository.getStudentsByTagsAndAttributes(
                attributes.toArray(new Integer[0]),
                tags.toArray(new Integer[0]),
                credentialsWithSchools.schoolIds().toArray(new Integer[0]),
                name,
                pageable.getPageSize(),
                ServiceUtils.getOffset(pageable)
        );
    }

    public Flux<Student> getStudentsByFilter(
            CredentialsWithSchools credentialsWithSchools,
            Long filterId,
            Pageable pageable
    ) {
        return filterRepository.findByIdAndCreatorIdOrIsPublic(filterId, credentialsWithSchools.credentials().id())
                .flatMapMany(f ->
                        getStudentsByExpression(
                                credentialsWithSchools,
                                f.expression(),
                                pageable
                        )
                )
                .switchIfEmpty(Mono.error(Errors.NotFoundException::new));
    }

    public Flux<Student> getStudentsByExpression(
            CredentialsWithSchools credentialsWithSchools,
            String expression,
            Pageable pageable
    ) {
        return Mono.just(filterValidator.getTagsFromExpression(expression))
                .flatMap(ids -> filterValidator.checkTagsArePresent(ids, credentialsWithSchools.credentials().id()))
                .then(Mono.just(filterValidator.checkSyntax(expression)))
                .thenMany(studentRepository.findAllByExpression(
                                filterValidator.convertExpression(expression),
                                credentialsWithSchools.schoolIds().toArray(Integer[]::new),
                                pageable.getPageSize(),
                                ServiceUtils.getOffset(pageable)
                        )
                );
    }

    public Flux<Contact> getContactsByFilter(
            String address,
            String filterName,
            Contact.ContactType type
    ) {
        return filterRepository.findByHumanReadableId(filterName)
                .log(null, Level.SEVERE)
                .zipWith(authRepository.findUserIdByContact(address, type)
                        .switchIfEmpty(Mono.error(Errors.UnauthorizedResourceAccessException::new))
                        .flatMap(id -> schoolRepository.findSchoolsByUserId(id)
                                .map(HighSchool::id)
                                .map(Long::intValue)
                                .collectList()
                                .map(schools -> Pair.of(id, schools))
                        )
                        .log(null, Level.SEVERE)
                )
                .switchIfEmpty(Mono.error(Errors.NotFoundException::new))
                .flatMapMany(t ->
                        Mono.just(filterValidator.getTagsFromExpression(t.getT1().expression()))
                                .flatMap(ids -> filterValidator.checkTagsArePresent(ids, t.getT2().getFirst()))
                                .thenMany(
                                        studentRepository.findContactsByExpression(
                                                filterValidator.convertExpression(t.getT1().expression()),
                                                t.getT2().getSecond().toArray(Integer[]::new),
                                                type
                                        )
                                )
                );
    }

}
