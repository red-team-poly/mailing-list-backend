package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.repositories.AttributesRepository;
import com.readteam.mailinglistbackend.repositories.models.Attribute;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Map;

@Service
public class AttributesService {

    private final AttributesRepository repository;

    public AttributesService(AttributesRepository repository) {
        this.repository = repository;
    }

    public Mono<Map<String, Collection<Attribute>>> getAttributes() {
        return repository
                .getAttributes()
                .collectMultimap(Attribute::title);
    }

}
