package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.model.FilterRequest;
import com.readteam.mailinglistbackend.repositories.TagRepository;
import com.readteam.mailinglistbackend.repositories.models.Credentials;
import com.udojava.evalex.Expression;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

@Service
public class FilterValidator {

    private final TagRepository tagRepository;

    public FilterValidator(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public Mono<Boolean> checkTagsInExpression(Credentials credentials, FilterRequest filterRequest) {
        return Mono.fromCallable(() -> {
                    var tagIds = getTagsFromExpression(filterRequest.expression());
                    if (!tagIds.equals(filterRequest.tags()))
                        throw new Errors.BadExpressionException("tags do not match expression");
                    return tagIds;
                })
                .flatMap(ids -> checkTagsArePresent(ids, credentials.id()));
    }

    public Mono<Boolean> checkTagsArePresent(Set<Long> ids, Long creatorId) {
        return tagRepository.findAllByIdsAndCreatorId(ids.toArray(Long[]::new), creatorId)
                .collectList()
                .map(foundIds -> {
                    ids.removeAll(new HashSet<>(foundIds));
                    if (!ids.isEmpty()) {
                        throw new Errors.BadExpressionException("can't find tags : %s".formatted(ids.toString()));
                    }
                    return true;
                });
    }

    public Set<Long> getTagsFromExpression(String expression) {
        return Arrays.stream(expression.split("[^\\d]+"))
                .filter(i -> !i.isEmpty())
                .map(Long::parseLong)
                .collect(Collectors.toSet());
    }

    public String convertExpression(String expression) {
        return expression
                .replaceAll("(?<=!)[\\d]+", "($0<>ALL(array_agg(st.tag_id)))")
                .replaceAll("[\\d]++(?!<)", "($0=ANY(array_agg(st.tag_id)))")
                .replaceAll("!", "")
                .replaceAll("&", "AND")
                .replaceAll("\\|", "OR");
    }

    public String checkSyntax(String expression) {
        if (expression.isBlank()) throw new Errors.BadExpressionException("blank expression");
        expression = expression
                .replaceAll("![\\d]++", "true")
                .replaceAll("[\\d]+", "false")
                .replaceAll("&", " && ")
                .replaceAll("\\|", " || ");
        try {
            new Expression(expression).eval();
        } catch (Exception e) {
            throw new Errors.BadExpressionException("bad syntax %s".formatted(expression));
        }
        return expression;
    }
}
