package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.repositories.SchoolRepository;
import com.readteam.mailinglistbackend.repositories.models.HighSchool;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Map;

@Service
public class SchoolService {
    private final SchoolRepository schoolRepository;

    public SchoolService(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    public Mono<Map<String, Collection<HighSchool>>> getSchools() {
        return schoolRepository
                .getSchools()
                .collectMultimap(HighSchool::instituteName);
    }

}