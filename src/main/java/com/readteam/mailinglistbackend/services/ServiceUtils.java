package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.errors.Errors;
import com.readteam.mailinglistbackend.model.PublicPermissions;
import com.readteam.mailinglistbackend.repositories.models.Authored;
import com.readteam.mailinglistbackend.repositories.models.Credentials;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

public class ServiceUtils {
    public static int getOffset(Pageable pageable) {
        return pageable.getPageNumber() * pageable.getPageSize();
    }

    public static <T extends Authored> T validateAuthor(Credentials credentials, T entity) {
        if (!Objects.equals(credentials.id(), entity.author())) {
            throw new Errors.UnauthorizedResourceAccessException();
        }
        return entity;
    }

    public static <T extends PublicPermissions> T validateAuthorPermissions(Credentials credentials, T request) {
        if (!credentials.permission() && request.getPublic()) {
            throw new Errors.PublicPermissionsException();
        }
        return request;
    }

    public static <T extends PublicPermissions, R extends PublicPermissions> T validateUpdatePermissions(T entity, R request) {
        if (entity.getPublic() && !request.getPublic()) {
            throw new Errors.ChangePublicPermissionsException();
        }
        return entity;
    }

    public static Mono<Boolean> checkUnique(Boolean isPublic, Supplier<Mono<Boolean>> exists) {
        var existsCheck = exists.get().map(t -> {
            if (t) throw new Errors.UniqueConstraintException();
            return true;
        });
        return isPublic ? existsCheck : Mono.just(true);
    }

    public static <T extends Authored, R extends PublicPermissions> Mono<T> updateWithAuthorAndUnique(
            Credentials credentials,
            R request,
            Long id,
            Supplier<Mono<Boolean>> exists,
            Function<Long, Mono<T>> findById,
            TriFunction<Long, R, Long, Mono<T>> updateOrCreate,
            Supplier<Mono<Boolean>> logicCheck
    ) {
        return Mono.just(validateAuthorPermissions(credentials, request))
                .then(checkUnique(request.getPublic(), exists))
                .then(logicCheck.get())
                .then(Mono.justOrEmpty(id))
                .flatMap(i -> findById.apply(i).switchIfEmpty(Mono.error(Errors.NotFoundException::new)))
                .map(t -> validateAuthor(credentials, t))
                .map(t -> validateUpdatePermissions(t, request))
                .flatMap(t -> updateOrCreate.apply(id, request, credentials.id()))
                .switchIfEmpty(updateOrCreate.apply(null, request, credentials.id()));
    }

    public static <T extends Authored, R extends PublicPermissions> Mono<T> updateWithAuthorAndUnique(
            Credentials credentials,
            R request,
            Long id,
            Supplier<Mono<Boolean>> exists,
            Function<Long, Mono<T>> findById,
            TriFunction<Long, R, Long, Mono<T>> updateOrCreate
    ) {
        return updateWithAuthorAndUnique(
                credentials,
                request,
                id,
                exists,
                findById,
                updateOrCreate,
                () -> Mono.just(true)
        );
    }

    public static <T extends Authored> Mono<T> deleteAuthored(
            Credentials credentials,
            Long id,
            Function<Long, Mono<T>> findById,
            Function<T, Mono<Void>> delete
    ) {
        return findById.apply(id)
                .map(t -> validateAuthor(credentials, t))
                .map(t -> {
                    if (t.getPublic()) throw new Errors.PublicItemDeletionException();
                    return t;
                })
                .flatMap(t -> delete.apply(t).then(Mono.just(t)))
                .switchIfEmpty(Mono.error(Errors.NotFoundException::new));
    }

    @FunctionalInterface
    interface TriFunction<A, B, C, R> {

        R apply(A a, B b, C c);

        default <V> TriFunction<A, B, C, V> andThen(
                Function<? super R, ? extends V> after) {
            Objects.requireNonNull(after);
            return (A a, B b, C c) -> after.apply(apply(a, b, c));
        }
    }

}
