package com.readteam.mailinglistbackend.services;

import com.readteam.mailinglistbackend.model.UserRequest;
import com.readteam.mailinglistbackend.model.UserResponse;
import com.readteam.mailinglistbackend.repositories.SchoolRepository;
import com.readteam.mailinglistbackend.repositories.UserRepository;
import com.readteam.mailinglistbackend.repositories.models.UserEntity;
import com.readteam.mailinglistbackend.repositories.models.UserWithSchools;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final SchoolRepository schoolRepository;

    public UserService(UserRepository userRepository, SchoolRepository schoolRepository) {
        this.userRepository = userRepository;
        this.schoolRepository = schoolRepository;
    }

    public Flux<UserEntity> getUsers(
            String name,
            Pageable pageable
    ) {
        return userRepository.getUsers(
                name,
                pageable.getPageSize(),
                ServiceUtils.getOffset(pageable)
        );
    }

    @Transactional
    public Mono<UserResponse> updateUser(
            Long id,
            UserRequest userRequest) {
        return userRepository.updatePermissions(id, userRequest.permission())
                .flatMap(e -> schoolRepository.removeSchools(e.id()).collectList().map(i -> e))
                .flatMap(userEntity -> schoolRepository.assignSchools(
                                        id,
                                        userRequest.schoolIds().toArray(new Integer[0])
                                )
                                .collectList()
                                .map(ids -> Pair.of(userEntity, ids))
                                .map(pair -> new UserResponse(pair.getFirst(), pair.getSecond()))
                );
    }

    public Mono<UserWithSchools> getUserWithSchools(Long id) {
        return userRepository.findById(id)
                .zipWith(schoolRepository.findSchoolsByUserId(id).collectList())
                .map(tuple2 -> new UserWithSchools(tuple2.getT1(), tuple2.getT2()));
    }

    public Mono<UserEntity> getUser(Long id) {
        return userRepository.findById(id);
    }
}
