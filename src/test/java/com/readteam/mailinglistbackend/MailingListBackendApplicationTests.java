package com.readteam.mailinglistbackend;

import com.readteam.mailinglistbackend.repositories.models.Contact;
import com.readteam.mailinglistbackend.repositories.models.Student;
import com.udojava.evalex.Expression;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


class MailingListBackendApplicationTests {

    @Test
    void contextLoads() {
        var c = Student.class.getDeclaredConstructors();
        System.out.println(Arrays.toString(c));
        System.out.println(Arrays.toString(Student.class.getDeclaredFields()));
    }


    @Test
    void bla() {
        var contact = "email";
        System.out.println(Contact.ContactType.valueOf(contact.toUpperCase()));
    }

    public int getFirstNotNull(MatchResult m) {
        for (int j = 1; j < m.groupCount(); j++) {
            if (m.group(j) != null) return j;
        }
        return -1;
    }
    @Test
    void blu() {
        var str = "!1&(5|3)";
        System.out.println(convertExpression(str));
    }

    @Test
    void bol() {
        var expression = "!1&(5|3)";

        expression = expression
                .replaceAll("![\\d]++", "false")
                .replaceAll("[\\d]+", "true")
                .replaceAll("&", " && ")
                .replaceAll("\\|", " || ")
        ;
        Expression exp = new Expression(expression + "");
        System.out.println(exp.eval());
    }

    public String convertExpression(String expression) {
        return expression
                .replaceAll("(?<=!)[\\d]+", "($0<>ALL(array_agg(st.tag_id)))")
                .replaceAll("[\\d]++(?!<)", "($0=ANY(array_agg(st.tag_id)))")
                .replaceAll("!", "")
                .replaceAll("&", "AND")
                .replaceAll("\\|", "OR");
    }

    @Test
    void shit() {
        var expression = "1400|(!5&!14)";

        System.out.println(convertExpression(expression));

    }


}
